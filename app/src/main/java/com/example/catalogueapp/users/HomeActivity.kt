package com.example.catalogueapp.users

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.catalogueapp.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}