package com.example.catalogueapp.welcome

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.catalogueapp.R

class OnboardingScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding_screen)

        //loading a default frag. for the frame layout
        supportFragmentManager.beginTransaction().replace(R.id.fragcontainer,WelcomeScreen1()).commit()

    }
}