package com.example.catalogueapp.welcome

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.catalogueapp.R


class WelcomeScreen3 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_welcome_screen3, container, false)

        val selection = view.findViewById<Button>(R.id.btnSelection)

        selection.setOnClickListener {
            //goes to next fragment
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.fragcontainer,SelectionFragment())
            transaction?.disallowAddToBackStack()
            transaction?.commit()
        }
        return view
    }


}