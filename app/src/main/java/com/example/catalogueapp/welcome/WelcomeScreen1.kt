package com.example.catalogueapp.welcome

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.catalogueapp.R


class WelcomeScreen1 : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_welcome_screen1, container, false)
        //view identification
        val btnSkip = view.findViewById<Button>(R.id.btnSkip)
        val btnNext = view.findViewById<Button>(R.id.btnNext)

        btnSkip.setOnClickListener {
            //goes to next fragment
            val transaction = activity?.supportFragmentManager?.beginTransaction()
            transaction?.replace(R.id.fragcontainer,SelectionFragment())
            transaction?.disallowAddToBackStack()
            transaction?.commit()
        }

        btnNext.setOnClickListener {
             //goes to next fragment
             val transaction = activity?.supportFragmentManager?.beginTransaction()
             transaction?.replace(R.id.fragcontainer,WelcomeScreen2())
            transaction?.disallowAddToBackStack()
            transaction?.commit()
        }


        return view
    }
}